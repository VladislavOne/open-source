// class Currency {
//   int Cur_ID;
//   int Cur_ParentID;
//   String Cur_Code;
//   String Cur_Abbreviation;
//   String Cur_Name;
//   String Cur_Name_Bel;
//   String Cur_Name_Eng;
//   String Cur_QuotName;
//   String Cur_QuotName_Bel;
//   String Cur_QuotName_Eng;
//   String Cur_NameMulti;
//   String Cur_Name_BelMulti;
//   String Cur_Name_EngMulti;
//   int Cur_Scale;
//   int Cur_Periodicity;
//   String Cur_DateStart;
//   String Cur_DateEnd;

//   Currency(
//       this.Cur_ID,
//       this.Cur_ParentID,
//       this.Cur_Code,
//       this.Cur_Abbreviation,
//       this.Cur_Name,
//       this.Cur_Name_Bel,
//       this.Cur_Name_Eng,
//       this.Cur_QuotName,
//       this.Cur_QuotName_Bel,
//       this.Cur_QuotName_Eng,
//       this.Cur_NameMulti,
//       this.Cur_Name_BelMulti,
//       this.Cur_Name_EngMulti,
//       this.Cur_Scale,
//       this.Cur_Periodicity,
//       this.Cur_DateStart,
//       this.Cur_DateEnd);

//   Currency.fromJson(Map<String, dynamic> json) {
//     Cur_ID = json['Cur_ID'];
//     Cur_ParentID = json['Cur_ParentID'];
//     Cur_Code = json['Cur_Code'];
//     Cur_Abbreviation = json['Cur_Abbreviation'];
//     Cur_Name = json['Cur_Name'];
//     Cur_Name_Bel = json['Cur_Name_Bel'];
//     Cur_Name_Eng = json['Cur_Name_Eng'];
//     Cur_QuotName = json['Cur_QuotName'];
//     Cur_QuotName_Bel = json['Cur_QuotName_Bel'];
//     Cur_QuotName_Eng = json['Cur_QuotName_Eng'];
//     Cur_NameMulti = json['Cur_NameMulti'];
//     Cur_Name_BelMulti = json['Cur_Name_BelMulti'];
//     Cur_Name_EngMulti = json['Cur_Name_EngMulti'];
//     Cur_Scale = json['Cur_Scale'];
//     Cur_Periodicity = json['Cur_Periodicity'];
//     Cur_DateStart = json['Cur_DateStart'].toString();
//     Cur_DateEnd = json['Cur_DateEnd'].toString();
//   }
// }

class RateShort {
  int curId;
  String date;
  num curOfficialRate;
  String curName;
  String curAbbreviation;

  RateShort(this.curId, this.date, this.curOfficialRate, this.curName,
      this.curAbbreviation);

  RateShort.fromjson(Map<String, dynamic> json) {
    curId = json['Cur_ID'];
    date = json['Date'].toString();
    curOfficialRate = json['Cur_OfficialRate'];
    curName = json['Cur_Name'];
    curAbbreviation = json['Cur_Abbreviation'];
  }
}
