import 'package:flutter/material.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.green,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage('assets/images/bank-background.jpg'),
              ),
            ),
            child: Text(
              'Side menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          ListTile(
            dense: true,
            leading: Icon(
              Icons.assignment,
              color: Colors.green[400],
            ),
            title: Text(
              'Валюта в мире',
              style: TextStyle(
                color: Color.fromRGBO(31, 79, 22, 1),
                fontSize: 26,
              ),
            ),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}
