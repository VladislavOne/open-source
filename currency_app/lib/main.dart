import 'package:currency_app/convertWidget.dart';
import 'package:currency_app/currences.dart';
import 'package:currency_app/currencyDrawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'operations.dart';

void main() => runApp(CurrencyApp());

class CurrencyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    setOrientDisplay();
    return MaterialApp(
      title: 'Валюта',
      home: HomePage(),
      theme: ThemeData(scaffoldBackgroundColor: Color.fromRGBO(1, 127, 98, 1)),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  List<RateShort> currencies = List<RateShort>();
  double rezult = 0;
  bool isConnect = false;
  bool isload = true;

  _loadCurrencies() {
    cheakToConnectInternet().then((internet) => {
          setState(() {
            if (internet != null && internet)
              isConnect = true;
            else
              isConnect = false;
          }),
          if (internet != null && internet)
            {
              fetchCurrences().then((value) {
                currencies.clear();
                setState(() => currencies.addAll(value));
              }),
              _loadData(),
            }
        });
  }

  _loadData() {
    setState(() {
      isload = false;
    });
  }

  _errorConnect() {
    return CupertinoAlertDialog(
      title: Text('Ошибка!'),
      content: Text('Нет подключения к Интернету'),
      actions: [
        CupertinoDialogAction(
          child: Text('Повторить'),
          onPressed: () => {cheakToConnectInternet()},
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    _loadCurrencies();
    return isConnect
        ? Scaffold(
            drawer: DrawerPage(),
            appBar: AppBar(
              centerTitle: true,
              title: Text(
                'Валюта сегодня',
                style: TextStyle(color: Colors.white, fontSize: 22),
              ),
              backgroundColor: Color.fromRGBO(1, 152, 117, 1),
            ),
            body: isload ? _progressBar() : _buildList(),
          )
        : _errorConnect();
  }

  Widget _progressBar() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildList() {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.white,
                width: 1,
              ),
            ),
          ),
          child: FlatButton(
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 20.0, bottom: 20.0, left: 0.0, right: 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        border: Border(
                            right: BorderSide(color: Colors.white, width: 1))),
                    height: 100,
                    width: 150,
                    child: Text(
                      '${index + 1} ' + currencies[index].curName,
                      style: TextStyle(
                        fontFamily: 'Rubic',
                        fontSize: 16,
                        color: Colors.grey[200],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5),
                  ),
                  Text(
                    currencies[index].curOfficialRate.toString() + ' BYN',
                    style: TextStyle(
                      fontFamily: 'Times New Roman',
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(0, 55, 42, 1),
                    ),
                  ),
                ],
              ),
            ),
            onPressed: () => Navigator.of(context).push(MaterialPageRoute<void>(
                builder: (_) => ConvertPage(
                      currencies: currencies,
                      index: index,
                    ))),
          ),
        );
      },
      itemCount: currencies.length,
    );
  }
}
