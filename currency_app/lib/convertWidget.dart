import 'package:flutter/material.dart';
import 'currences.dart';

class ConvertPage extends StatefulWidget {
  @override
  _ConvertPage createState() =>
      _ConvertPage(currencies: this.currencies, index: this.index);

  ConvertPage({this.currencies, this.index});

  final List<RateShort> currencies;
  final int index;
}

class _ConvertPage extends State<ConvertPage> {
  _ConvertPage({this.currencies, this.index});

  final List<RateShort> currencies;
  final int index;
  String _abbriviature = 'BYN';
  bool isReverse = true;
  double rezult = 0;
  double rezultBuf = 0;
  @override
  Widget build(context) {
    return _convertWindow();
  }

  _calculateValueCurrency(double value, int index) async {
    if (isReverse) {
      setState(() {
        rezult = value * currencies[index].curOfficialRate;
        rezultBuf = value;
      });
    } else {
      setState(() {
        rezult = value / currencies[index].curOfficialRate;
        rezultBuf = value;
      });
    }
  }

  _convertWindow() {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Конвертатор',
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
        backgroundColor: Color.fromRGBO(1, 152, 117, 1),
      ),
      body: Container(
        decoration:
            BoxDecoration(border: Border.all(color: Colors.white, width: 5)),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                '${currencies[index].curName}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 40,
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                decoration: BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: Colors.white, width: 3)),
                ),
                margin:
                    EdgeInsets.only(top: 30, bottom: 0, left: 35, right: 35),
                child: TextField(
                  scrollPadding: EdgeInsets.all(5.0),
                  decoration: InputDecoration(border: InputBorder.none),
                  textAlign: TextAlign.center,
                  onChanged: (value) =>
                      _calculateValueCurrency(double.parse(value), index),
                  cursorColor: Colors.white,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                  ),
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 30),
              ),
              FloatingActionButton(
                onPressed: () => {
                  if (_abbriviature == 'BYN')
                    {
                      setState(() {
                        isReverse = false;
                        _abbriviature = currencies[index]
                            .curAbbreviation
                            .toString()
                            .toUpperCase();
                      }),
                      _calculateValueCurrency(rezultBuf, index),
                    }
                  else
                    {
                      setState(() {
                        isReverse = true;
                        _abbriviature = 'BYN'.toString();
                      }),
                      _calculateValueCurrency(rezultBuf, index),
                    }
                },
                child: Icon(
                  Icons.cached,
                  color: Colors.black,
                ),
                backgroundColor: Colors.white,
                shape: BeveledRectangleBorder(),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 20),
              ),
              Text(
                '${rezult.toStringAsFixed(rezult.truncateToDouble() == rezult ? 0 : 3)} $_abbriviature',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 36,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
