import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'currences.dart';

class AllCurrencies {
  List<RateShort> currencies = List<RateShort>();
  bool _loading = true;
  String url = 'https://www.nbrb.by/api/exrates/currencies';

  _loadDataFromInternet() {}

  Widget _progressBar() {
    return Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _allCurrencies() {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Мировая валюта',
          style: TextStyle(color: Colors.white, fontSize: 22),
        ),
        backgroundColor: Color.fromRGBO(1, 152, 117, 1),
      ),
      body: _loading ? _progressBar() : _buildList(),
    );
  }

  Widget _buildList() {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Container(
          child: Padding(
            padding: const EdgeInsets.only(
                top: 20.0, bottom: 20.0, left: 0.0, right: 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(color: Colors.white, width: 1))),
                  height: 100,
                  width: 150,
                  child: Text(
                    '${index + 1} ' + currencies[index].curName,
                    style: TextStyle(
                      fontFamily: 'Rubic',
                      fontSize: 16,
                      color: Colors.grey[200],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                ),
                Text(
                  currencies[index].curAbbreviation + ' BYN',
                  style: TextStyle(
                    fontFamily: 'Times New Roman',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(0, 55, 42, 1),
                  ),
                ),
              ],
            ),
          ),
        );
      },
      itemCount: currencies.length,
    );
  }
}
