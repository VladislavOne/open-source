import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'currences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';

Future<List<RateShort>> fetchCurrences() async {
  var date = DateTime.now();
  String url =
      "https://www.nbrb.by/api/exrates/rates?ondate=$date&periodicity=0";
  var response = await http.get(url);

  var currences = List<RateShort>();

  if (response.statusCode == 200) {
    var currency = json.decode(response.body);

    for (var item in currency) {
      currences.add(RateShort.fromjson(item));
    }
  } else {
    Exception("Faild to load?");
  }

  return currences;
}

Future<bool> cheakToConnectInternet() async {
  var connectivity = await (Connectivity().checkConnectivity());
  if (connectivity == ConnectivityResult.wifi ||
      connectivity == ConnectivityResult.mobile)
    return true;
  else
    return false;
}

void setOrientDisplay() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
}
